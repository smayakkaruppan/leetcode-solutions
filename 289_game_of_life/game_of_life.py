
#According to the Wikipedia's article: "The Game of Life, also known simply as Life,
## is a cellular automaton devised by the British mathematician John Horton Conway in 1970."

#Given a board with m by n cells, each cell has an initial state live (1) or dead (0).
#Each cell interacts with its eight neighbors (horizontal, vertical, diagonal)
## using the following four rules (taken from the above Wikipedia article):

#Any live cell with fewer than two live neighbors dies, as if caused by under-population.
#Any live cell with two or three live neighbors lives on to the next generation.
#Any live cell with more than three live neighbors dies, as if by over-population..
#Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
#Write a function to compute the next state (after one update) of the board given its current state.
#The next state is created by applying the above rules simultaneously to every cell in the current state,
## where births and deaths occur simultaneously.


def next_state(board):
    rows = len(board)
    cols = len(board[0])
    neighbours = []
    for r in range(rows):
        rowlist = []
        for c in range(cols):
            live_neighbours = 0
            if r-1 >= 0 and c-1 >= 0:
                live_neighbours = live_neighbours + board[r-1][c-1]
            if r-1 >= 0:
                live_neighbours = live_neighbours + board[r-1][c]
            if r-1 >= 0 and c+1 < cols:
                live_neighbours = live_neighbours + board[r-1][c+1]
            if c-1 >= 0:
                live_neighbours = live_neighbours + board[r][c-1]
            if c+1 < cols:
                live_neighbours = live_neighbours + board[r][c+1]
            if r+1 < rows and c-1 >= 0:
                live_neighbours = live_neighbours + board[r+1][c-1]
            if r+1 < rows:
                live_neighbours = live_neighbours + board[r+1][c]
            if r+1 < rows and c+1 < cols:
                live_neighbours = live_neighbours + board[r+1][c+1]
            rowlist.append(live_neighbours)
        neighbours.append(rowlist)
    for r in range(rows):
        for c in range(cols):
            if neighbours[r][c] < 2:
                board[r][c] = 0
            elif neighbours[r][c] > 3:
                board[r][c] = 0
            elif neighbours[r][c] == 3:
                board[r][c] = 1
    return board

board = [
  [0,1,0],
  [0,0,1],
  [1,1,1],
  [0,0,0]
]

output = next_state(board)
print(output)